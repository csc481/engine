define(['../element', '../styles/default'], function(Element, Default) {

    /**
     * A select provides the user with several options, one of which they will choose.
     * Choices should be an array of objects that contain a key and a value, ala:
     * [{key: "wolf", text: "Select Wolf Character"}]
     *
     * Broadcasts the event 'selected' when an option is chosen.
     */
    class Select extends Element {
        constructor(choices) {
            super(new Default.Select());
            // set up the different choices
            this.choices = choices;
            this.selected = 0;
            // set up the events we'll use to change/enact things.
            this._bindEvents();
        }

        _bindEvents() {
            // events used to bind to alter this element
            this.events.subscribe("next", this.next.bind(this));
            this.events.subscribe("prev", this.prev.bind(this));
            this.events.subscribe("confirm", this.confirm.bind(this));
        }

        /** Selects the next choice */
        next() {
            this.selected++;
            if (this.selected >= this.choices.length)
                this.selected = 0;
        }

        /** Selects the previous choice */
        prev() {
            this.selected--;
            if (this.selected < 0)
                this.selected = this.choices.length - 1;
        }

        /**
         * Adds keybindings to this select for all the available controls.
         * @param input The Input object to attach keybindings to.
         * @param prev A set of keybinds for previous
         * @param next A set of keybinds for next
         * @param confirm A set of keybinds for confirm
         */
        bindNavigationKeys(input, prev, next, confirm) {
            for (let i = 0; i < prev.length; i++)
                input.onKeyDown(prev[i], this._makeBroadcaster("prev"))
            for (let i = 0; i < next.length; i++)
                input.onKeyDown(next[i], this._makeBroadcaster("next"));
            for (let i = 0; i < confirm.length; i++)
                input.onKeyDown(confirm[i], this._makeBroadcaster("confirm"));
        }

        _makeBroadcaster(event) {
            return function() {
                this.events.broadcast(event);
            }.bind(this);
        }

        /** Returns the height of this select in pixels */
        getHeight() {
            let height = (this.choices.length - 1) * this.style.choice.height;
            height += (this.choices.length - 1) * this.style.margin;
            height += this.style.selected.height;
            return height;
        }

        /** When the user confirms a specific selection */
        confirm() {
            this.events.broadcast("selected", this.choices[this.selected]);
        }

        /**
         * Draws the options being selected.
         * @param ctx The context to draw on.
         * @param startX The x position in pixels
         * @param startY The y position in pixels
         */
        draw(ctx, startX, startY) {
            ctx.save();
            let x = startX;
            let y = startY;
            for (let i = 0; i < this.choices.length; i++) {
                ctx.save();
                // swap style if it is selected or not
                let height = 0;
                if (i === this.selected) {
                    this.style.selected.apply(ctx);
                    height = this.style.selected.height;
                } else {
                    this.style.choice.apply(ctx);
                    height = this.style.choice.height;
                }
                // add the margin to the y coord if we are not the first
                if (i !== 0)
                    y += this.style.margin;
                ctx.fillText(this.choices[i].text, x, y);
                // update positions with height
                y += height;
                ctx.restore();
            }
            ctx.restore();
        }
    }

    return Select;
});