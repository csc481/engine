define(['../element', '../styles/default'], function(Element, Default) {

    /**
     * An element is a part of the GUI that can have a style applied to it.
     * All GUI elements can produce events for things that happen to them.
     * Almost all UI elements are driven by their event buses for interactivity.
     *
     * Elements work similar to entities, but are specifically for the GUI.
     */
    class Background extends Element {
        /**
         * Creates a new background
         */
        constructor() {
            super(new Default.Background());
        }

        /** Must be fully implemented!
         * At the moment this simply draws a new template background.
         * @param ctx the context to be drawn on
         * @param x the x position to draw from
         * @param y the y position to draw from
         * @param width the width of the background
         * @param height the height of the background
         */
        draw(ctx, x, y, width, height) {
            ctx.save();
            this.style.apply(ctx);
            ctx.fillRect(x, y, width, height);
            ctx.restore();
        }
    }

    return Background;
});
