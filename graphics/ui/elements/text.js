define(['../element', '../styles/default'], function(Element, Default) {

    /**
     * A text element merely displays text at a specified location.
     */
    class Text extends Element {
        constructor(text) {
            super(new Default.Text());
            this.text = text;
        }
        /**
         * Sets the text associate d with this object.
         */
        setText(text) {
            this.text = text;
        }

        /**
         * Returns the text associated with this object.
         */
        getText() {
            return this.text;
        }

        /**
         * Draws the text element
         * @param ctx The context to draw on.
         * @param ui The User Interface it will be drawn on
         * @param x The x position in pixels
         * @param y The y position in pixels
         */
        draw(ctx, x, y) {
            ctx.save();
            this.style.apply(ctx, Text.name);
            ctx.fillText(this.text, x, y);
            ctx.restore();
        }
    }

    return Text;
});
