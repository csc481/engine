define(['../element', '../styles/default'], function(Element, Default) {

    /**
     * A counter allows the user to choose a number between a minimum and maximum.
     */
    class Counter extends Element {
        /**
         * Creates a new counter.
         * @param min Minimum value allowed. null for infinite.
         * @param max Maximum value allowed. null for infinite.
         * @param initial The initial value.
         * @param prompt (o) The prompt for the counter.
         */
        constructor(min, max, initial, prompt) {
            super(new Default.Heading());
            // set up the different choices
            this.min = min;
            this.max = max;
            this.current = initial;
            this.prompt = prompt;
            // set up the events we'll use to change/enact things.
            this._bindEvents();
        }

        _bindEvents() {
            // events used to bind to alter this element
            this.events.subscribe("next", this.next.bind(this));
            this.events.subscribe("prev", this.prev.bind(this));
            this.events.subscribe("confirm", this.confirm.bind(this));
        }

        /** Selects the next choice */
        next() {
            this.current++;
            if (this.max !== null && this.current > this.max)
                this.current = this.min;
        }

        /** Selects the previous choice */
        prev() {
            this.current--;
            if (this.min !== null && this.current < this.min)
                this.current = this.max;
        }

        /**
         * Adds keybindings to this select for all the available controls.
         * @param input The Input object to attach keybindings to.
         * @param prev A set of keybinds for previous
         * @param next A set of keybinds for next
         * @param confirm A set of keybinds for confirm
         */
        bindNavigationKeys(input, prev, next, confirm) {
            for (let i = 0; i < prev.length; i++)
                input.onKeyDown(prev[i], this._makeBroadcaster("prev"))
            for (let i = 0; i < next.length; i++)
                input.onKeyDown(next[i], this._makeBroadcaster("next"));
            for (let i = 0; i < confirm.length; i++)
                input.onKeyDown(confirm[i], this._makeBroadcaster("confirm"));
        }

        _makeBroadcaster(event) {
            return function() {
                this.events.broadcast(event);
            }.bind(this);
        }

        /** When the user confirms a specific selection */
        confirm() {
            this.events.broadcast("selected", this.current);
        }

        /**
         * Draws the options being selected.
         * @param ctx The context to draw on.
         * @param x The x position in pixels
         * @param y The y position in pixels
         */
        draw(ctx, x, y) {
            ctx.save();
            this.style.apply(ctx);
            let text = this.current;
            if (this.prompt)
                text = this.prompt + ": " + text;
            ctx.fillText(text, x, y);
            ctx.restore();
        }
    }

    return Counter;
});