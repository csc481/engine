define([], function() {

    /**
     * A Style can be applied to a GUI element. Styles affect how the GUI looks.
     * Styles store global properties that are always applied and properties that are only applied
     * to specific elements.
     */
    class Style {
        constructor() {
            /** All the properties that are always applied */
            this.globals = {};
            /** All the properties sorted by their type */
            this.typed = {};
        }

        /**
         * Adds a global property to this style.
         * @param property The name of the property on the ctx to modify
         * @param value The value that should be assigned to that property during application, functions may be given
         * @return Style this for chaining
         */
        addGlobal(property, value) {
            this.globals[property] = value;
            return this;
        }

        /**
         * Adds an element property to this style. Only relevant if the element uses them.
         * @param property Property name to add.
         * @param value Value to add it to.
         */
        addElement(property, value) {
            this[property] = value;
        }

        /**
         * Adds a typed property to this style. Will override global styles.
         * @param type The string type of element to only apply these properties to
         * @param property The name of the property on the ctx to modify
         * @param value The value that should be assigned to that property during application, functions may be given
         * @return Style this for chaining
         */
        addTyped(type, property, value) {
            if (!(type in this.typed))
                this.typed[type] = {};
            this.typed[type][property] = value;
            return this;
        }

        /**
         * Applies the style to a graphics context.
         * @param ctx the canvas context to apply the style to
         * @param type (o) The type of object the style is being applied to by name.
         */
        apply(ctx, type) {
            // apply all the global styles.
            this._applyGlobal(ctx);
            // apply type specific styles if a type was provided.
            if (type)
                this._applyType(ctx, type);
        }

        _applyGlobal(ctx) {
            for (let key in this.globals)
                Style._ApplyProperty(ctx, key, this.globals[key]);
        }

        _applyType(ctx, type) {
            // only apply if it actually exists
            if (!(type in this.typed))
                return;
            for (let key in this.typed[type])
                Style._ApplyProperty(ctx, key, this.typed[type][key]);
        }

        static _ApplyProperty(ctx, property, value) {
            // if it's a function, invoke it.
            if (value instanceof Function)
                value = value();
            // set the result on the context
            ctx[property] = value;
        }
    }

    return Style;
});