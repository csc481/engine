/**
 * The Default Style -- "Mochimus"
 * This style uses the same kind of style that the engine documentation and branding uses.
 */
define(['../style'], function(Style) {

    /**
     * Default style for normal text.
     */
    class Text extends Style {
        constructor() {
            super();

            this.addGlobal("font", "32px tahoma");
            this.addGlobal("fillStyle", "#f8f8f8");
            this.addGlobal("textAlign", "center");

            // element only props
            this.addElement("height", 32);
        }
    }

    /**
     * Default style for headings
     */
    class Heading extends Text {
        constructor() {
            super();
            // font is larger
            this.addGlobal("font", "48px tahoma");
            this.addElement("height", 48);
        }
    }

    /** Contains all the style information to make a select work */
    class Select extends Style {
        constructor() {
            super();

            this.addElement("choice", new Text());
            this.addElement("selected", new Heading());
            this.addElement("margin", 10);
        }

    }

    class Background extends Style {
        constructor() {
            super();
            // has a dark background by default
            this.addGlobal("fillStyle", "#3a3a3a");
        }
    }

    return {
        Text: Text,
        Heading: Heading,
        Background: Background,
        Select: Select
    };
});