define(['../manager', './sprites'], function(Manager, Sprites) {

    /**
     * The system used for drawing appearance components.
     */
    class DrawManager extends Manager {
        /**
         * Clears the screen of a given game.
         * @param ctx the context to be cleared
         * @param game an object that lets us see the game width and height
         */
        clear(ctx, game) {
            ctx.clearRect(0, 0, game.width, game.height);
        }

        /**
         * Subscribes this object to any events having to do with drawing and sets the scene
         * @param scene the scene to be set
         * @param events the events object to subscribe to.
         */
        init(scene, events) {
            events.subscribe("draw", this.draw.bind(this));
            this.scene = scene;
        }

        /**
         * Draws the collection of entities attached to the scene or passed in.
         * @param ctx Context to draw on
         * @param entities? optional entities to draw.
         */
        draw(ctx, entities) {
            if (entities == null)
                entities = this.scene.entities;
            // loop through all the entities that can be drawn
            for (let i = 0; i < entities.length; i++) {
                let entity = entities[i];
                if (!entity.has(Sprites.Sprite.Name))
                    continue;
                entity.sprite.draw(ctx, entity.x, entity.y, entity.sprite.width, entity.sprite.height, entity.rotation);
            }
        }
    }
    DrawManager.Name = "draw";

    /**
     * Similar to the DrawManager, except it has a unit system wrapped around it.
     */
    class UnitDrawManager extends DrawManager {
        constructor(unitSystem) {
            super();
            this.us = unitSystem;
        }

        /**
         * Clears the entire screen given a context and a certain game.
         * @param ctx the context to clear from
         * @param game the game that contains the height and width of the game
         */
        clear(ctx, game) {
            ctx.clearRect(0, 0, this.us.unitsToPixelsX(game.width), this.us.unitsToPixelsY(game.height));
        }

        /**
         * Draws the collection of entities attached to the scene or passed in.
         * @param ctx Context to draw on
         * @param entities? optional entities to draw.
         */
        draw(ctx, entities) {
            if (entities == null)
                entities = this.scene.entities;
            for (let i = 0; i < entities.length; i++) {
                let entity = entities[i];
                if (!entity.has(Sprites.Sprite.Name))
                    continue;
                entity.sprite.draw(ctx,
                    this.us.unitsToPixelsX(entity.x), this.us.unitsToPixelsY(entity.y),
                    this.us.unitsToPixelsX(entity.sprite.width), this.us.unitsToPixelsY(entity.sprite.height),
                    entity.rotation
                );
            }
        }
    }

    return {
        DrawManager: DrawManager,
        UnitDrawManager: UnitDrawManager
    };

});
