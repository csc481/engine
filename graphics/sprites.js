// Define dependencies
define(['./../component', '../content/asset'], function (Component, Asset) {
    /**
     * The class that can be used to interact with sprites.
     * @param path/asset The path to the image file or an asset file
     * @param data Object that includes attributes like width/height
     * @constructor
     */
    class Sprite extends Component {
        constructor(asset, data) {
            super();
            /** The image asset for this sprite */
            if (typeof asset === 'string' || asset instanceof String)
                this.image = new Asset.Asset(asset, Asset.Types.image);
            else
                this.image = asset;
            /** The width the sprite will display */
            this.width = data.width;
            /** The height the sprite will display */
            this.height = data.height;
            if(data.anchor != null) {
                this.anchorX = this.anchorXDecode(data.anchor.x);
                this.anchorY = this.anchorYDecode(data.anchor.y);
                if ('rotation' in data.anchor)
                    this.anchorRot = data.anchor.rotation;
                else
                    this.anchorRot = 0;
            } else {
                this.anchorX = 0;
                this.anchorY = 0;
                this.anchorRot = 0;
            }
        }

        /**
         * Decodes the anchor point given, and returns the value that it's equal to for the X direction.
         * @param anchorPoint
         * @returns {*}
         */
        anchorXDecode(anchorPoint){
            if(anchorPoint === 'LEFT'){
                return 0;
            }else if(anchorPoint === 'MIDDLE'){
                return .5;
            }else if(anchorPoint === 'RIGHT'){
                return 1;
            }else if(anchorPoint == null){
                return 0;
            }else {
                return anchorPoint;
            }
        }

        /**
         * Decodes the anchor point given, and returns the value that it's equal to for the Y direction.
         * @param anchorPoint
         * @returns {*}
         */
        anchorYDecode(anchorPoint){
            if(anchorPoint === 'TOP'){
                return 0;
            }else if(anchorPoint === 'MIDDLE'){
                return .5;
            }else if(anchorPoint === 'BOTTOM'){
                return 1;
            }else if(anchorPoint == null){
                return 0;
            }else {
                return anchorPoint;
            }

        }
        /**
        * Initializes the sprite so it is attached to a given entity.
        * @param entity the entity this sprite will be a part of.
        */
        init(entity){
            this.entity = entity;
        }

        /**
         * Returns a promise object for when the sprite is loaded and begins
         * loading the sprite asynchronously.
         * Resolves immediately if already loaded.
         */
        load() {
            return this.image.load();
        }

        /**
         * Draws the given sprite assuming it is loaded.
         * @param ctx The context to draw this image on
         * @param x The x position to draw this sprite at
         * @param y The y position to draw this sprite at
         * @param width The width of the sprite
         * @param height The height of the sprite
         * @param rot? The optional radians to rotate the sprite
         */
        draw(ctx, x, y, width, height, rot=0) {
            if (!this.image.isLoaded())
                throw "Image cannot be drawn. Please call load on asset: " + this.image;
            if (width == null)
                width = this.width;
            if (height == null)
                height = this.height;
            ctx.save();
            ctx.translate(x, y);
            ctx.rotate(rot + this.anchorRot);
            ctx.drawImage(this.image.data, -(this.anchorX * width), -(this.anchorY * height), width, height);
            ctx.restore();
        }
    }
    Sprite.Name = "sprite";

    /**
     *The class that can be used to animate sprites.
     * @param path The path to an image file.
     * @param data The object that includes width for each frame.
     */
    class AnimatedSprite extends Sprite {
        constructor(asset, data) {
            super(asset, data);
            /** The width for each frame. */
            this.frameWidth = data.frameWidth;
            /** The height of the animation. */
            this.animationHeight = data.animationHeight;
            /** Number of frames total */
            this.frameTotal = data.frameTotal || 1;
            /** Number of animations total */
            this.animationTotal = data.animationTotal;
            /** The current frame that the animation is on. */
            this.frameIndex = 0;
            /** The number of updates since the current frame was loaded. */
            this.tickCount = 0;
            /** The ticks that the user wants the animation to run at. */
            this.tickMax = data.tick;
            /** The current animation playing. */
            this.animation = 0;
        }
        /**
         * Draws the object to the screen at the point. This also will clear any last pieces of animation in the
         * @param ctx The context to draw the object on.
         * @param x The x position the object will be placed on.
         * @param y The y position the object will be placed on.
         */
        draw(ctx, x, y, width, height, rot=0){
            if (!this.image.isLoaded())
                throw "Image cannot be drawn. Please call load on asset: " + this.image;
            ctx.save();
            ctx.translate(x, y);
            ctx.rotate(rot + this.anchorRot);
            ctx.drawImage(
                this.image.data,
                // sx, sy where to crop on image
                (this.frameWidth * this.frameIndex),
                this.animationHeight * this.animation,
                // sWidth, sHeight the width of the rectangle
                this.frameWidth,
                this.animationHeight,
                // dx, dy the coordinate on destination canvas (x/y)
                -(this.anchorX * width),
                -(this.anchorY * height),
                // dWidth, dHeight
                width, height);
            ctx.restore();
            // TODO fix this update, it isn't real-time feasible
            this.update();
        }

        /**
         * Updates the index of the frame, or loops back if the frame is going to be higher than the total number of frames.
         */
        update(){
            this.tickCount++;
            if (this.tickCount > this.tickMax - 1 && this.frameIndex < this.frameTotal - 1){
                this.frameIndex++;
                this.tickCount = 0;
            } else if(this.tickCount > this.tickMax - 1 &&!(this.frameIndex < this.frameTotal - 1)){
                this.frameIndex = 0;
                this.tickCount = 0;
            }
        }

        /**
         * Switches the current animation to the one passed as a parameter.
         * @param animNum The index of the animation to switch to.
         */
        switchAnimations(animNum){
            if(animNum <= this.animationTotal - 1){
                this.animation = animNum;
            }else{
                throw "Index out of bounds";
            }


        }


    }
    AnimatedSprite.Name = "sprite";


    /**
     * The class used to animate sprites with multiple images as animations.
     */
    class AnimatedSpriteMultipleImages extends Component{
        /**
         *
         * @param data The data used to tell the
         */
        constructor(data) {
            super();
            this.spriteList = [];

            /** Number of animations total */
            this.animationTotal = 0;
            /** The current frame that the animation is on. */
            this.frameIndex = 0;
            /** The number of updates since the current frame was loaded. */
            this.tickCount = 0;
            /** The current animation playing. */
            this.animation = [];
        }

        addSprite(data, path){
            var spriteAnim = [];
            /** The image used for the sprite sheet. */
            var image = new Asset.Asset(path, Asset.Types.image);
            /** The width of the sprite sheet. */
            var width = data.width;
            /** The height of the sprite sheet */
            var height = data.height;
            /** The width for each frame. */
            var frameWidth = data.frameWidth;
            /** Number of frames total */
            var frameTotal = data.frameTotal || 1;

             /** Pushes all the values into an array that will be pushed into the main array. */
            spriteAnim.push(image);
            spriteAnim.push(width);
            spriteAnim.push(height);
            spriteAnim.push(frameWidth);
            spriteAnim.push(frameTotal);
            this.spriteList.push(spriteAnim);
            this.animationTotal++;

        }

        /**
         * Returns a promise object for when the sprite is loaded and begins
         * loading the sprite asynchronously.
         * Resolves immediately if already loaded.
         */
        load(){
            return this.animation[0].load();
        }

        /**
         * Updates the index of the frame, or loops back if the frame is going to be higher than the total number of frames.
         */
        update(){
            if(this.frameIndex < this.animation[4]-1){
                this.frameIndex++;
            } else {
                this.frameIndex = 0;
            }


        }
        /**
         * Switches the current animation to the one passed as a parameter.
         * @param animNum The index of the animation to switch to.
         */
        switchAnimations(animNum){
            if(animNum < this.animationTotal-1){
                this.animation = this.spriteList[animNum];
            }else{
                throw RangeException("Index out of bounds.");
            }


        }


    }


// Return exposed functions/classes.
    return {
        Sprite: Sprite,
        AnimatedSprite: AnimatedSprite
    };
});
