define([], function() {

    /**
     * This class is used to produce a game server using NodeJS. This class requires nodejs to invoke it.
     */
    class GameServer {
        constructor(port) {
            this.port = port;
        }

        /**
         * Sets up a web server with sockets.
         * @returns {GameServer} for chaining
         */
        setupWebServer() {
            let express = require('express');
            let app = express();
            let http = require('http').Server(app);
            let io = require('socket.io')(http);
            let path = require('path');

            this.app = app;
            this.http = http;
            this.io = io;
            this.path = path;
            this.express = express;

            return this;
        }

        /** Begins the game server on the port given */
        listen() {
            this.http.listen(this.port, function(){
                console.log('listening on *:' + this.port);
            }.bind(this));
        }
    }

    return GameServer;
});