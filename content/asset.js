define(['../game'], function(game) {

    class UnsupportedTypeException {
        constructor(message) {
            this.message = message;
        }
    }

    /**
     * This class is used by the Engine (not exposed to the user)
     * It determines what assets the engine is willing to load.
     */
    class AssetType {
        constructor(name, extensions, loader) {
            this.name = name;
            this.extensions = extensions;
            this.loader = loader;
        }

        /**
         * Checks if this asset type supports the given file type
         */
        supports (fileType) {
            for (var i = 0; i < this.extensions.length; i++)
                if (fileType === this.extensions[i])
                    return true;
            return false;
        }

        /**
         * Attempts to grab load a Json string given a path to it.
         * @param path path to load from
         */
        static LoadJson(path) {
            return new Promise((resolve, reject) => {
                var xhr = new XMLHttpRequest();
                xhr.open("GET", path);
                xhr.onload = () => resolve(JSON.parse(xhr.responseText));
                xhr.onError = () => reject(xhr);
                xhr.send();
            });
        }

        /**
         * Unlike load JSON, this method uses JsonP instead of an XHR request.
         * The benefit is that we can load local files without restrictions from
         * CORS.
         * @param path path to load
         */
        static LoadJsonP(path) {
            return new Promise((resolve, reject) => {
                require([path], function(data) {
                    resolve(data);
                }, function(error) {
                    reject(error);
                });
            });
        }

        /**
         * Attempts to load an image file given a path to load form.
         * @param path path to load from
         *
         */
        static LoadImage(path) {
            return new Promise((resolve, reject) => {
                var image = new Image();
                image.onload = () => resolve(image);
                image.onerror = (evt) => reject(evt);
                // Set the source to begin loading, effectively xhr.send
                image.src = path;
            });
        }
    }

    /**
     * The class that allows loading of files, data or images.
     * @param path The path to the data file.
     * @param data (o) The supported type (from TYPES)
     * @constructor
     */
    class Asset {
        constructor(path, type) {
            this.data = null;
            this.path = path;
            if (type === undefined)
                type = Asset._inferType(path);
            this.type = type;
            if (this.type === null)
                throw new UnsupportedTypeException(path + " is an unsupported file. " +
                    "If incorrect, please provide explicit type to Asset from Asset.TYPES.")
        }

        /**
         * Loads this asset, returns a promise object that will resolve
         * when the data has been loaded.
         */
        load() {
            // If it's already loaded, just return an immedaitely resolved promise
            if (this.isLoaded())
                return new Promise(function (resolve, reject) {
                    resolve(this.data);
                }.bind(this));

            // Otherwise use the type's loader to load it.
            return this.type.loader(this.path).then(function(data) {
                this.data = data;
            }.bind(this));
        }

        /**
         * Returns true if this asset has been loaded.
         * @returns {boolean}
         */
        isLoaded() {
            return this.data != null;
        }

        /** toString override for telling basic info. */
        toString() {
            return "Asset [" + this.path + ", " + this.type.name + "]";
        }

        /**
         * Grabs data from the path when not directly given from user.
         */
        static _inferType(path) {
            var ext = path.split('.').pop().toLowerCase();
            for (var type in Asset.TYPES) {
                if (Asset.TYPES[type].supports(ext))
                    return Asset.TYPES[type];
            }
            return null;
        }
    }
    /** The available types of assets that this engine can use */
    Asset.TYPES = {
        data: new AssetType('data', ['json', 'js'], AssetType.LoadJsonP),
        // Optional different type for non-local data using XHR
        webData: new AssetType('webData', ['json', 'js'], AssetType.LoadJson),
        image: new AssetType('image', ['png', 'jpeg', 'jpg'], AssetType.LoadImage)
    };

    /**
     * Convenience function that loads an array of assets and returns a promise
     * object for when all of the assets are loaded.
     * @return Promise A promise for when all assets are done, errors on single failure.
     */
    function LoadAllAssets(assets) {
        return new Promise((resolve, reject) => {
            // The tally of the progress on our loading.
            var result = {
                loaded: 0,
                failed: 0,
                failures: []
            };
            // if assets is empty, load immediately.
            if (assets.length === 0) {
                resolve(result);
                return;
            }
            // The function callback for when everything has been tallied
            var checkFinish = function() {
                // Make sure we've finished
                if (result.loaded + result.failed !== assets.length)
                    return false;
                // Check if we have finished and all of them succeeded
                if (result.failed > 0) {
                    reject(result);
                    return true;
                }
                resolve(result);
                return true;
            };
            // Go through each asset and attempt to load.
            for (var i = 0; i < assets.length; i++) {
                var asset = assets[i];
                asset.load().then(function () {
                    result.loaded += 1;
                    checkFinish();
                }).catch(function() {
                    result.failed += 1;
                    result.failures.push(asset);
                    checkFinish();
                });
            }
        });
    }

    // Return exposed functions/classes.
    return {
        Asset: Asset,
        Types: Asset.TYPES,
        LoadAllAssets: LoadAllAssets
    };
});
