define([], function() {
    /**
     * The component class is one of the core parts of the engine because it makes up an Entity.
     * All elements and features of an entity are stored in components. Components have certain functions
     * that must be supplied in order to successfully be implemented.
     *
     * This class works more like an interface or a reference to assist in creating components.
     */
    class Component {
        constructor() {
            /** Whether or not this component has been initialized */
            this.initialized = false;
        }
        /**
         * Components must have an init function. Usually this is used to set options on
         * entity that are necessary for the given component.
         * @param entity The entity that this component has been added to.
         */
        init(entity, eventBus, components) {}
    }
    /** @required Components must have a unique name */
    Component.Name = "component";

    return Component;
});