define([], function() {

    /**
     * An event bus is used to manage a local event system.
     * Used for messaging between components in the Entity class, but can
     * also be used for any local part-based event handling.
     *
     * The name 'all' (KeyAll constant) is reserved to broadcast to all names.
     */
    class EventBus {
        constructor() {
            // handlers: name => object[event] => [callbacks]
            this.handlers = {};
            this.currentId = 0;
        }

        /**
         * Broadcasts to a specific channel (or all if name is not specified) for an event occurrence.
         * @param event The name of the event that occurred.
         * @param data The data that should be pass
         * @param name? The name of the channel (or null)
         */
        broadcast(event, data, name) {
            if (name == null) {
                this.broadcastAll(event, data);
            } else {
                this._broadcast(event, data, name);
                // Also broadcast to all the subscribers in all
                this._broadcast(event, data, EventBus.All);
            }
        }

        /**
         * Broadcasts to all namespaces an event, including subscribers to all.
         * @param event The event to broadcast
         * @param data The data to deliver to the callback
         */
        broadcastAll(event, data) {
            // All channels that have this event will broadcast for it, including all subscribers.
            for (let name in this.handlers) {
                if (event in this.handlers[name])
                    this._broadcast(event, data, name);
            }
        }

        _broadcast(event, data, name) {
            // Make sure the event has subscribers to broadcast to
            if (!(name in this.handlers) || !(event in this.handlers[name]))
                return;
            // call all its callbacks with our data.
            for (let id in this.handlers[name][event]) {
                this.handlers[name][event][id](data);
            }
        }

        /**
         * Subscribe to an event with a callback that will be invoked when the specified event
         * occurs. Order is not guaranteed or preserved.
         * @param event The name of the event to subscribe to
         * @param callback The callback to invoke
         * @param name? optional name for a sub-channel -> all will go to every channel
         * @return int The id of the subscribed callback for removing
         */
        subscribe(event, callback, name) {
            // perform setup if none has occurred yet.
            if (name == null)
                name = EventBus.All;
            this._create(event, name);
            // otherwise attach the callback
            this.handlers[name][event][++this.currentId] = callback;
            // return the id of the callback
            return this.currentId;
        }

        /**
         * Unsubscribes from an event given the id that returns from subscribe
         * and the event/optional name originally used to subscribe.
         * @param event The name of the event to unsub from.
         * @param id The id returned from subscribe for the callback.
         * @param name? (optional) The name of the sub channel.
         */
        unsubscribe(event, id, name) {
            if (name == null)
                name = EventBus.All;
            if (name in this.handlers && event in this.handlers[name] && id in this.handlers[name][event])
                delete this.handlers[name][event][id];
            else
                throw "Invalid id, name, or event. Cannot unsubscribe " + id + ".";
        }

        /** Creates the entries in handlers if they do not exist */
        _create(event, name) {
            if (!(name in this.handlers)) {
                // Create new handler and create a new set of callbacks.
                this.handlers[name] = {};
                this.handlers[name][event] = {}; // object-arrays
                return;
            }
            if (!(event in this.handlers[name])) {
                this.handlers[name][event] = {}; // object array
            }
        }
    }
    /** Key used for broadcasting to all handlers */
    EventBus.All = "all";


    return EventBus;
});