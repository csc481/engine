// Define dependencies
define([], function() {

/**
 * InputHandler is used to bind functions to input events
 * from the browser.
 *
 * All key events pass the event.
 * Mouse events pass the event, the coordinates (matching canvas), and the change in x/y.
 *
 * @param game The game object we are tracking input for.
 * @constructor
 */
var InputHandler = function(game) {
    /** Keep track of the game we are collecting input for */
    this.game = game;
    /** Maintains a list of callback bindings */
    this.bindings = {};
    this.resetBindings();
    /** Maintains a list of keys currently down */
    this.keysDown = {};
    /** Maintains whether or not the mouse is down and its coordinates */
    this.mouse = {
        x: 0,
        y: 0
    };
    this.mouseDown = false;
};
/** Resets all the bindings of this input handler to be empty
 * @return All the old bindings. */
InputHandler.prototype.getBindings = function() {
    return this.bindings;
};
/** Returns the old bindings set before this one, useful for swapping */
InputHandler.prototype.swapBindings = function(bindings) {
    let old = this.bindings;
    this.bindings = bindings;
    return old;
};
/** Removes all bindings, returns all those that existed before. */
InputHandler.prototype.resetBindings = function() {
    let old = this.bindings;
    this.bindings = {};
    this.bindings.key = {};
    this.bindings.keyDown = {};
    this.bindings.keyUp = {};
    this.bindings.click = [];
    this.bindings.mouseDown = [];
    this.bindings.mouseUp = [];
    this.bindings.move = [];
    return old;
};
/** Bind a callback that happens on mouse click */
InputHandler.prototype.onClick = function(callback) {
    this.bindings.click.push(callback);
};
/** Bind a callback that happens on move */
InputHandler.prototype.onMove = function(callback) {
    this.bindings.move.push(callback);
};
/** Bind a callback for when the given key is pressed */
InputHandler.prototype.onKeyPress = function(key, callback) {
    // Initialize key in case multiple same key bindings
    if (!(key in this.bindings.key))
        this.bindings.key[key] = [];
    this.bindings.key[key].push(callback);
};
InputHandler.prototype.onKeyDown = function(key, callback) {
    if (!(key in this.bindings.keyDown))
        this.bindings.keyDown[key] = [];
    this.bindings.keyDown[key].push(callback);
};
InputHandler.prototype.onKeyUp = function(key, callback) {
    if (!(key in this.bindings.keyUp))
        this.bindings.keyUp[key] = [];
    this.bindings.keyUp[key].push(callback);
};
/** Bind a callback for when the mouse is down */
InputHandler.prototype.onMouseDown = function(callback) {
    this.bindings.mouseDown.push(callback);
};
/** Bind a callback for when the mouse is released */
InputHandler.prototype.onMouseUp = function(callback) {
    this.bindings.mouseUp.push(callback);
};
/** Initializes the input handler, makes the listeners */
InputHandler.prototype.init = function() {
    addEventListener("keypress", this._handleKeyPressEvent.bind(this));
    addEventListener("keydown", this._handleKeyDown.bind(this), false);
    addEventListener("keyup", this._handleKeyUp.bind(this), false);
    addEventListener("mousemove", this._handleMove.bind(this));
    addEventListener("click", this._handleClick.bind(this));
    addEventListener("mousedown", this._handleMouseDown.bind(this));
    addEventListener("mouseup", this._handleMouseUp.bind(this));
};
/** Handles mouse click events */
InputHandler.prototype._handleClick = function(event) {
    this._handleMouseEvent(this.bindings.click, event);
};
/** Handles mouse move events */
InputHandler.prototype._handleMove = function(event) {
    this._handleMouseEvent(this.bindings.move, event);
};
/** Internal function that handles invoking callbacks */
InputHandler.prototype._handleMouseEvent = function(callbacks, event) {
    var coords = this._convertCoords(event.clientX, event.clientY);
    // Update the mouse we track as well.
    var dx = coords.x - this.mouse.x;
    var dy = coords.y - this.mouse.y;
    this.mouse.x = coords.x;
    this.mouse.y = coords.y;
    for (var i = 0; i < callbacks.length; i++) {
        callbacks[i](event, coords.x, coords.y, dx, dy);
    }
};
/** Converts the client x/y coords to match the canvas */
InputHandler.prototype._convertCoords = function(clientX, clientY) {
    var client = this.game.canvas.getBoundingClientRect();
    var scaleX = this.game.width / client.width;
    var scaleY = this.game.height / client.height;
    return {
        x: (clientX - client.left) * scaleX,
        y: (clientY - client.top) * scaleY
    }
};
/** Internal function that handles key function invocations */
InputHandler.prototype._handleKeyPressEvent = function(event) {
    this._handleKeyEvent(event, this.bindings.key);
};
/** Unlike key event, this function focuses on maintaining keys down */
InputHandler.prototype._handleKeyDown = function(event) {
    this.keysDown[event.key] = true;
    this._handleKeyEvent(event, this.bindings.keyDown);
};
/** The match for key down except to release */
InputHandler.prototype._handleKeyUp = function(event) {
    delete this.keysDown[event.key];
    this._handleKeyEvent(event, this.bindings.keyUp);
};
InputHandler.prototype._handleKeyEvent = function(event, aggregation) {
    // Only check for bindings if we have any
    if (!(event.key in aggregation))
        return;
    // Invoke all bindings for this key
    for (var i = 0; i < aggregation[event.key].length; i++) {
        aggregation[event.key][i](event);
    }
};
/** Handles when the mouse goes down */
InputHandler.prototype._handleMouseDown = function(event) {
    this.mouseDown = true;
    this._handleMouseEvent(this.bindings.mouseDown, event);
};
/** Handles when the mouse goes up */
InputHandler.prototype._handleMouseUp = function(event) {
    this.mouseDown = false;
    this._handleMouseEvent(this.bindings.mouseUp, event);
};

return {
    InputHandler: InputHandler
};
});