define(['./three.min.js'], function(THREE) {
    if (typeof window !== 'undefined')
        window.THREE = THREE;
    return THREE;
});