# Mochi Game Engine

The Mochi Game Engine written in JavaScript.
Included alongside games in any way that it can be accessed.

# Setup
Setting up the Mochi engine from source is easy. Just download the source and place it alongside or inside of the game
you wish to make with the engine.

Our engine uses requirejs for imports/exports since ES6 modules are not yet fully implemented in all browsers. Simply
include the engine's top level package to use all its components.

```javascript
// Assumes Mochi is set up alongside the game directory in a directory called engine
requirejs(['../engine/Mochi' ..... );
```

## Networking Setup
If you plan to use the Mochi engine for networked games, you will need to install additional dependencies.
The Mochi engine is set up to be used within Node, but doesn't come pre-downloaded with all the necessary components.

Network setup requires that nodejs has been installed with the node package manager.

Simply use the node package manager to install the dependencies within the engine:

```
// From within the engine directory
npm install
```

This command will download all the node components needed for network functionality. You can now write a simple game
server just by including the MochiServer top level package:

```javascript
var Mochi = require('../engine/MochiServer');
```

This module contains all of the Mochi engine along with additional server-only components.


# Architecture
The Mochi engine uses the Entity-Component-Manager architecture and is structured accordingly. Most subdirectories
(such as graphics, events, physics) contain a component (sprite, behavior, body) and a manager (the ones that end in manager).

Unlike Entity-Component-System (ECS), ECM is a slight variation where components are not purely data-driven,
but can instead have simple logic and operations embedded. Additionally, components can technically use the event system
inside each entity to gain whatever information from other components. Thus, the term is manager instead of system since
they are managing components that have logic instead of performing all available operations.

# Libraries Used

## RequireJS

[Source](http://requirejs.org/)

This library was included because ES6 import/export modules have not been standardized in all browsers.

## Node JS

[Source](https://nodejs.org/en/)

This library was included to allow for networked games. We use additional dependencies with node:

 * [Socket.io](https://socket.io/): For socket communication
 * [Express](https://expressjs.com/): For webserver set up and management
