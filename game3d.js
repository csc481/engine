define(['./game', './libraries/three'], function(Game, Three) {

    /**
     * A version of Game that works for 3d games using three js integration
     */
    class Game3d extends Game {

        /**
         * Overrides the graphics setup function from game and instead starts a three.js canvas
         */
        _setUpGraphics() {
            this.log("Generating three js renderer.");
            // The context is the three js renderer
            this.ctx = new Three.WebGLRenderer();
            // the renderer will be used to create the canvas
            this.ctx.setSize(this.getPixelWidth(), this.getPixelHeight());
            this.canvas = this.ctx.domElement;
            // create the canvas using this renderer
            document.body.appendChild(this.canvas);
            this.canvas.focus();
        }
    }

    return Game3d;
});