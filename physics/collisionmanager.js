define(['../manager', './../events/eventbus', './body'], function(Manager, EventBus, Body) {

    class CollisionManager extends Manager {
        /**
         * Creates a new collision manager.
         * @param x The x position for the quad tree to start at.
         * @param y the y position for the quad tree to start at.
         * @param width the width of the quad tree.
         * @param height the height of the quadtree.
         * @param minWidth the minimum width of one of the branches the quadtree.
         * @param minHeight the mininmum height for one of the branches of the quadtree.
         */
        constructor(x, y, width, height, minWidth, minHeight) {
            super();
            this.events = new EventBus();
            this.quadtree = new QuadTree(x, y, width, height, minWidth || 32, minHeight || 32, this.events);
        }
        /**
         * Initializes the quad tree.
         * @param scene The scene to be initialized.
         * @param events the events that must be subscribed to.
         */
        init(scene, events) {
            // bind adding/removing entities and update
            events.subscribe("update", this.updateThenCollide.bind(this));
            events.subscribe("entity_add", this.add.bind(this));
            events.subscribe("entity_remove", this.remove.bind(this));
        }
        /**
         * Adds an entity to the tree.
         * @param entity the entity to be added.
         */
        add(entity) {
            if (!entity.has(Body.Name))
                return;
            this.quadtree.add(entity);
            this.events.broadcast("add_entity", entity);
        }
        /**
         * Removes an entity from the tree.
         * @param the entity to be removed.
         */
        remove(entity) {
            if (!entity.has(Body.Name))
                return;
            this.quadtree.remove(entity);
            this.events.broadcast("remove_entity", entity);
        }

        /** used for event binding convenience */
        updateThenCollide(ms) {
            this.update(ms);
            this.collide();
        }
        /**
         * Called whenever there is a collision between objects
         */
        collide() {
            this.quadtree.collide();
        }
        /**
         * defines dt and updates by it.
         */
        update(ms) {
            let dt = ms/1000;
            this.quadtree.update(dt);
        }

        /**
         * This can be used to update only one entity. It confers lookup costs, and so shouldn't be used often
         * @param entity The entity to update
         */
        updateOne(entity, ms) {
            let dt = ms/1000;
            this.quadtree.updateOne(entity, dt);
        }

        /**
         * Similar to the above, this checks collisions for one entity. Use sparingly.
         * @param entity The entity to check collision with
         */
        collideOne(entity) {
            this.quadtree.collideOne(entity);
        }
    }
    CollisionManager.Name = "collision";

    class QuadTree {
        constructor(x, y, width, height, minWidth, minHeight, eventBus) {
            this.events = eventBus;
            this.root = new QuadTreeNode(null, x, y, width, height, minWidth, minHeight, this.events);
        }

        add(entity) {
            this.root.add(entity);
            this.root.finalize();
        }

        remove(entity) {
            this.root.remove(entity);
        }

        update(ms) {
            this.root.update(ms);
            this.root.finalize();
        }

        updateOne(entity, ms) {
            this.root.updateOne(entity, ms);
            this.root.finalize();
        }

        collide() {
            this.root.collide();
            this.root.finalize();
        }

        collideOne(entity) {
            this.root.collideOne(entity);
            this.root.finalize();
        }
    }

    class QuadTreeNode {
        constructor(parent, x, y, width, height, minWidth, minHeight, eventBus) {
            this.parent = parent;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.data = [];
            this.tempData = [];
            this.events = eventBus;
            // If we're not small enough yet
            if (width > minWidth && height > minHeight) {
                var midWidth = width / 2;
                var midHeight = height / 2;
                // Subdivide this node into 4 sub nodes
                this.children = [
                    new QuadTreeNode(this, x, y, midWidth, midHeight, minWidth, minHeight, this.events),
                    new QuadTreeNode(this, x + midWidth, y, midWidth, midHeight, minWidth, minHeight, this.events),
                    new QuadTreeNode(this, x, y + midHeight, midWidth, midHeight, minWidth, minHeight, this.events),
                    new QuadTreeNode(this, x + midWidth, y + midHeight, midWidth, midHeight, minWidth, minHeight, this.events)
                ];
            } else {
                // Otherwise, this will be a leaf
                this.children = null;
            }
        }

        contains(rectangle) {
            // In order to not have collisions between boxes, each edge can only be in one box
            // So, I'm making the boxes have their top and left edges, but not their bottom and right ones
            return this.x <= rectangle.x && this.y <= rectangle.y
                && this.x + this.width >= rectangle.x + rectangle.width // That's why these are > not >=
                && this.y + this.height >= rectangle.y + rectangle.height;
        }

        move(entity) {
            var box = entity.body.getBoundingBox();
            // If you don't still fit in this node
            if (!this.contains(box)) {
                // Go up a level, and keep going up until you do fit, or you reach the root
                if (this.parent !== null) {
                    this.parent.move(entity);
                } else {
                    // If we're at the root, just take the entity
                    this.tempData.push(entity);
                }
            } else {
                // Add it starting at this node (either add to this, or one of it's children)
                this.add(entity);
            }
        }

        update(ms) {
            // Update and move all the entities at this node
            for (var i = 0; i < this.data.length; i++) {
                this.data[i].body.update(ms);
                let entity = this.data[i];
                this.data.splice(i, 1);
                i--;
                if (entity !== undefined) {
                    this.move(entity);
                }
            }
            // Go to the children and ask there too
            if (this.children !== null) {
                for (var k = 0; k < 4; k++) {
                    this.children[k].update(ms);
                }
            }
        }

        updateOne(entity, ms) {
            // If we found the entity
            if (this.data.includes(entity)) {
                // Update it
                entity.body.update(ms);
                this.data.splice(this.data.indexOf(entity), 1);
                // See where it needs to go
                if (entity !== undefined) {
                    this.move(entity);
                }
                // Otherwise, check your child for the entity
            } else if (this.children !== null) {
                var box = entity.body.getBoundingBox();
                // pick which child to attempt to add it to by seeing where it's (x,y) pair is,
                // if it doesn't fit in there, then it's not going into any of the children
                var topBottom = box.y >= this.y + this.height / 2;
                var leftRight = box.x >= this.x + this.width / 2;
                // indices are:
                // 00 01
                // 10 11
                var i = topBottom << 1 | leftRight;
                this.children[i].updateOne(entity, ms);
            }
        }

        add(entity) {
            var box = entity.body.getBoundingBox();
            // It doesn't fit in this node
            if (!this.contains(box)) {
                // We're not the root, return false
                if (this.parent !== null) {
                    return false;
                }
                // We are the root, no one else will take it, so we'll take it
                this.tempData.push(entity);
                return true;
            }
            if (this.children !== null) {
                // pick which child to attempt to add it to by seeing where it's (x,y) pair is,
                // if it doesn't fit in there, then it's not going into any of the children
                var topBottom = box.y >= this.y + this.height / 2;
                var leftRight = box.x >= this.x + this.width / 2;
                // indices are:
                // 00 01
                // 10 11
                var index = topBottom << 1 | leftRight;
                if (this.children[index].add(entity)) {
                    return true;
                }
            }
            // None of our children took the entity, so we have to.
            this.tempData.push(entity);
            return true;
        }

        finalize() {
            Array.prototype.push.apply(this.data, this.tempData);
            this.tempData = [];
            if (this.children !== null) {
                for (let i = 0; i < 4; i++) {
                    this.children[i].finalize();
                }
            }
        }

        remove(entity) {
            // If this node has it
            let index = this.data.indexOf(entity);
            let index2 = this.tempData.indexOf(entity);
            if (index >= 0) {
                // Find the entity
                this.data.splice(index, 1);
            } else if (index2 >= 0) {
                // Check the tempData as well
                this.tempData.splice(index, 1);
                // Otherwise, ask it's child if it's there.
            } else if (this.children !== null) {
                var box = entity.body.getBoundingBox();
                // pick which child to attempt to add it to by seeing where it's (x,y) pair is,
                // if it doesn't fit in there, then it's not going into any of the children
                var topBottom = box.y >= this.y + this.height / 2;
                var leftRight = box.x >= this.x + this.width / 2;
                // indices are:
                // 00 01
                // 10 11
                var i = topBottom << 1 | leftRight;
                this.children[i].remove(entity);
            }
        }

        // An entity can collide with everything at it's node, and it's nodes children
        getAllCollisionCandidates() {
            // Initialize new list
            var totalList = [];
            // Add everything at this node
            Array.prototype.push.apply(totalList, this.data);
            // Get all your children's entities
            if (this.children !== null) {
                for (var i = 0; i < 4; i++) {
                    Array.prototype.push.apply(totalList, this.children[i].getAllCollisionCandidates());
                }
            }
            return totalList;
        }

        collide() {
            // If there's anything to check at this node
            if (this.data.length !== 0) {
                // Find everything that could be colliding
                var candidates = this.getAllCollisionCandidates();
                for (var i = 0; i < this.data.length; i++) {
                    for (var j = 0; j < candidates.length; j++) {
                        /* TODO: This is a hacky fix. To properly fix, I believe we should remove this body.onCollision
                         * part, and update games to handle things based on the event postceding that. The main issue here
                         * is that the other entity can't resolve the collision if the first gets removed during its resolution
                         */
                        if (this.data[i] == null) {
                            break;
                        }
                        if (this.data[i] !== candidates[j]) {
                            // Check collisions
                            if (this.data[i].body.collision(candidates[j].body)) {
                                // Resolve
                                this.data[i].body.onCollision(candidates[j]);
                                candidates[j].body.onCollision(this.data[i]);
                                this.events.broadcast("collision", {
                                    victim: candidates[j],
                                    culprit: this.data[i]
                                });
                            }
                        }
                    }
                    // Place entity at correct spot
                    let entity = this.data[i];
                    this.data.splice(i, 1);
                    i--;
                    if (entity !== undefined) {
                        this.move(entity);
                    }
                }
            }
            // Go to the children and ask there too
            if (this.children !== null) {
                for (var k = 0; k < 4; k++) {
                    this.children[k].collide();
                }
            }
        }


        collideOne(entity) {
            // If this node has it
            if (this.data.includes(entity)) {
                // Find everything that could be colliding
                var candidates = this.getAllCollisionCandidates();
                let flag = false;
                for (var j = 0; j < candidates.length; j++) {
                    // Check if it is
                    if (entity.body.collision(candidates[j].body)) {
                        // Resolve
                        entity.body.onCollision(candidates[j]);
                        flag = true;
                        // broadcast the single collision
                        this.events.broadcast("collision", {
                            victim: candidates[j],
                            culprit: entity
                        });
                    }
                }
                this.data.splice(this.data.indexOf(entity, 1));
                if (entity !== undefined) {
                    this.move(entity);
                }
                return flag;
                // Otherwise, ask it's child if it's there.
            } else if (this.children !== null) {
                var box = entity.body.getBoundingBox();
                // pick which child to attempt to add it to by seeing where it's (x,y) pair is,
                // if it doesn't fit in there, then it's not going into any of the children
                var topBottom = box.y >= this.y + this.height / 2;
                var leftRight = box.x >= this.x + this.width / 2;
                // indices are:
                // 00 01
                // 10 11
                var i = topBottom << 1 | leftRight;
                return this.children[i].collideOne(entity);
            }
        }
    }

    return CollisionManager;

});
