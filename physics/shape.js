define([], function() {
    class Point {
        constructor() {
        }

        getBoundingBox(x, y, rotation) {
            return {x: x, y: y, width: 1, height: 1};
        }

        collision(x, y, rotation, x2, y2, rotation2, shape) {
            return x === x2 && y === y2;
            if (shape instanceof Point) {
                return x === x2 && y === y2;
            } else if (shape instanceof Circle) {
                return Math.pow((x - x2), 2) + Math.pow((y - y2), 2) <= Math.pow((shape.r), 2);
            } else if (shape instanceof AxisAlignedBox) {
                return x >= x2 && x <= x2 + shape.width && y >= y2 && y <= y2 + shape.height;
            } else if (shape instanceof Box) {
                let corners = shape.getRotatedCorners(rotation2);
                return shape.pointInRect(x2-x, y2-y, corners);
            }
        }

        drawBounds(ctx, x, y, rotation) {
            ctx.strokeRect(x, y, 1, 1);
        }

    }

    class Circle {
        constructor (r) {
            this.r = r;
        }

        getBoundingBox(x, y, rotation) {
            return {x:x-this.r, y:y-this.r, width:2*this.r, height:2*this.r};
        }

        collision (x, y, rotation, x2, y2, rotation2, shape) {
            if (shape instanceof Point) {
                return Math.pow((x - x2), 2) + Math.pow((y - y2), 2) <= Math.pow((this.r), 2);
            } else if (shape instanceof Circle) {
                return Math.pow((x - x2), 2) + Math.pow((y - y2), 2) <= Math.pow((this.r + shape.r), 2);
            } else if (shape instanceof AxisAlignedBox) {
                let distX = Math.abs(x - x2);
                let distY = Math.abs(y - y2);

                if (distX > (shape.width/2 + this.r)) { return false; }
                if (distY > (shape.height/2 + this.r)) { return false; }

                if (distX <= (shape.width/2)) { return true; }
                if (distY <= (shape.height/2)) { return true; }

                let cornerDistance_sq = Math.pow(distX - shape.width/2, 2) +
                    Math.pow(distY - shape.height/2, 2);

                return (cornerDistance_sq <= Math.pow(this.r, 2));
            } else if (shape instanceof Box) {
                // Simple explanation, rotate circle, and do like axis aligned
                let sin = Math.sin(-rotation2);
                let cos = Math.cos(-rotation2);

                let cx = x - x2;
                let cy = y - y2;
                cx = cx*cos - cy*sin;
                cy = cx*sin + cy*cos;

                let distX = Math.abs(cx);
                let distY = Math.abs(cy);

                if (distX > (shape.width/2 + this.r)) { return false; }
                if (distY > (shape.height/2 + this.r)) { return false; }

                if (distX <= (shape.width/2)) { return true; }
                if (distY <= (shape.height/2)) { return true; }

                let cornerDistance_sq = Math.pow(distX - shape.width/2, 2) +
                    Math.pow(distY - shape.height/2, 2);

                return (cornerDistance_sq <= Math.pow(this.r, 2));
            }
        }

        drawBounds(ctx, x, y, rotation) {
            ctx.beginPath();
            ctx.arc(x,y,this.r,0,2*Math.PI);
            ctx.stroke();
            ctx.closePath()
        }
    }

    class AxisAlignedBox {
        constructor (width, height) {
            this.width = width;
            this.height = height;
        }

        getBoundingBox(x, y, rotation) {
            return {x:x-(this.width/2), y:y-(this.height/2), width:this.width, height:this.height};
        }

        collision (x, y, rotation, x2, y2, rotation2, shape) {
            if (shape instanceof Point) {
                return x2 >= x && x2 <= x + width && y2 >= y && y2 <= y + height;
            } else if (shape instanceof Circle) {
                let distX = Math.abs(x2 - x);
                let distY = Math.abs(y2 - y);

                if (distX > (this.width/2 + shape.r)) { return false; }
                if (distY > (this.height/2 + shape.r)) { return false; }

                if (distX <= (this.width/2)) { return true; }
                if (distY <= (this.height/2)) { return true; }

                let cornerDistance_sq = Math.pow(distX - this.width/2, 2) +
                    Math.pow(distY - this.height/2, 2);

                return (cornerDistance_sq <= Math.pow(shape.r, 2));
            } else if (shape instanceof AxisAlignedBox) {
                return x <= x2 + shape.width && x + this.width >= x2 && y <= y2 + shape.height && y + this.height >= y2;
            } else if (shape instanceof Box) {
                // TODO: Box to box collision
            }
        }

        drawBounds(ctx, x, y, rotation) {
            ctx.strokeRect(x-this.width/2, y-this.height/2, this.width, this.height);
        }
    }

    class Box {
        constructor (width, height) {
            this.width = width;
            this.height = height;
        }

        getRotatedCorners (rotation) {
            let topRight = [this.width/2, this.height/2];
            let topLeft = [-this.width/2, this.height/2];
            let bottomLeft = [-this.width/2, -this.height/2];
            let bottomRight = [this.width/2, -this.height/2];

            let sin = Math.sin(rotation);
            let cos = Math.cos(rotation);
            let corners = [
                    [
                        topRight[0]*cos - topRight[1]*sin,
                        topRight[0]*sin + topRight[1]*cos
                    ],
                    [
                        topLeft[0]*cos - topLeft[1]*sin,
                        topLeft[0]*sin + topLeft[1]*cos
                    ],
                    [
                        bottomLeft[0]*cos - bottomLeft[1]*sin,
                        bottomLeft[0]*sin + bottomLeft[1]*cos
                    ],
                    [
                        bottomRight[0]*cos - bottomRight[1]*sin,
                        bottomRight[0]*sin + bottomRight[1]*cos
                    ]
                ];
            return corners;
        }

        getBoundingBox(x, y, rotation) {
            let corners = this.getRotatedCorners(rotation);

            let minX = Math.min(corners[0][0], corners[1][0], corners[2][0], corners[3][0]);
            let maxX = Math.max(corners[0][0], corners[1][0], corners[2][0], corners[3][0]);

            let minY = Math.min(corners[0][1], corners[1][1], corners[2][1], corners[3][1]);
            let maxY = Math.max(corners[0][1], corners[1][1], corners[2][1], corners[3][1]);

            return {x:minX+x, y:minY+y, width:maxX-minX, height:maxY-minY};
        }

        pointInRect(x, y, corners) {
            let apX = x - corners[0][0];
            let apY = y - corners[0][1];

            let abX = corners[1][0] - corners[0][0];
            let abY = corners[1][1] - corners[0][1];

            let adX = corners[3][0] - corners[0][0];
            let adY = corners[3][1] - corners[0][1];

            let apDOTab = apX*abX + apY*abY;
            let abDOTab = abX*abX + abY*abY;

            let apDOTad = apX*adX + apY*adY;
            let adDOTad = adX*adX + adY*adY;

            return (0 < apDOTab && abDOTab) || (0 < apDOTad && apDOTad < adDOTad);
        }

        collision (x, y, rotation, x2, y2, rotation2, shape) {
            if (shape instanceof Point) {
                let corners = this.getRotatedCorners(rotation);
                return this.pointInRect(x-x2, y-y2, corners);
            } else if (shape instanceof Circle) {
                // Simple explanation, rotate circle, and do like axis aligned
                let sin = Math.sin(-rotation);
                let cos = Math.cos(-rotation);

                let cx = x2 - x;
                let cy = y2 - y;
                cx = cx*cos - cy*sin;
                cy = cx*sin + cy*cos;

                let distX = Math.abs(cx);
                let distY = Math.abs(cy);

                if (distX > (this.width/2 + shape.r)) { return false; }
                if (distY > (this.height/2 + shape.r)) { return false; }

                if (distX <= (this.width/2)) { return true; }
                if (distY <= (this.height/2)) { return true; }

                let cornerDistance_sq = Math.pow(distX - this.width/2, 2) +
                    Math.pow(distY - this.height/2, 2);

                return (cornerDistance_sq <= Math.pow(shape.r, 2));
            } else if (shape instanceof AxisAlignedBox) {
                // TODO: Box to box collision
            } else if (shape instanceof Box) {
                // TODO: This should be similar to above
            }
        }

        drawBounds(ctx, x, y, rotation) {
            let corners = getRotatedCorners(rotation);
            ctx.beginPath();
            ctx.moveTo(corners[0][0] + x, corners[0][1] + y);
            ctx.lineTo(corners[1][0] + x, corners[1][1] + y);
            ctx.lineTo(corners[2][0] + x, corners[2][1] + y);
            ctx.lineTo(corners[3][0] + x, corners[3][1] + y);
            ctx.lineTo(corners[0][0] + x, corners[0][1] + y);
            ctx.stroke();
            ctx.closePath();
        }
    }

    return {
        Point: Point,
        Circle: Circle,
        AxisAlignedBox: AxisAlignedBox,
        Box: Box
    };
});