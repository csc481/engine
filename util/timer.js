define(['./../events/eventbus'], function(EventBus) {

    /**
     * A timer can be used to repeatedly make a callback or just a single use.
     * It keeps track of time in-game through an update loop.
     */
    class Timer {
        /**
         * Timers are simple and can be used for many different purposes.
         * @param tick The time until the callback is called in-game seconds.
         * @param callback The function to invoke upon reaching allocated time or an event bus to fire events on.
         * @param data? The data to pass to the callback or event. If Event, an 'event' field should be included.
         * @param repeat? default true, whether or not this timer will keep calling after it fires.
         */
        constructor(tick, callback, data={}, repeat=true) {
            this.tick = tick * 1000;
            this.current = 0;
            if (callback instanceof EventBus) {
                if ('name' in data)
                    this.eventName = data.name;
                else
                    this.eventName = "timer";
                this.eventBus = callback;
            } else {
                this.callback = callback;
            }
            this.data = data;
            this.repeat = repeat;
            this.notSpent = true;
            this.paused = false;
        }

        /**
         * Updates the timer's tick.
         * @param ms The amount of time since last update.
         */
        update(ms) {
            if (this.paused)
                return;
            this.current += ms;
            if (this.current >= this.tick && this.notSpent) {
                this.current -= this.tick;
                if (!this.repeat)
                    this.notSpent = false;

                if (this.eventBus)
                    this.eventBus.broadcastAll(this.eventName, this.data);
                else
                    this.callback(this.data);
            }
        }

        pause() {
            this.paused = true;
        }

        unpause() {
            this.paused = false;
        }
    }

    return Timer;
});