define([], function() {

    /**
     * A basic unit system. Given the number of units that make up the width, it will handle
     * conversions between pixels and units. Uses width
     *
     * Used to keep physics in check despite multiple resolutions.
     */
    class UnitSystem {
        /**
         * Defines a unit system for objects in a game.
         * @param units the units to use
         * @param width the width of the units
         */
        constructor(units, width) {
            this.units = units;
            this.setWidth(width);
        }
        /**
         * Sets the width of the units and the pixelsPerUnit of the units
         * @param width the width to be set.
         */
        setWidth(width) {
            this.width = width;
            if (this.units)
                this.pixelsPerUnit = this.width / this.units;
        }
        /**
         * Sets the units for the game system
         * @param units the units to be set
         */
        setUnits(units) {
            this.units = units;
            if (this.width)
                this.pixelsPerUnit = this.width / this.units;
        }

        /** Converts pixels to units */
        pixelsToUnits(px) {
            return Math.floor(px / this.pixelsPerUnit);
        }

        /** Converts game units to pixels, usually for drawing */
        unitsToPixels(units) {
            return units * this.pixelsPerUnit;
        }

        pixelsToUnitsX(px) {return this.pixelsToUnits(px);}
        pixelsToUnitsY(px) {return this.pixelsToUnits(px);}
        unitsToPixelsX(units) {return this.unitsToPixels(units);}
        unitsToPixelsY(units) {return this.unitsToPixels(units);}


    }

    return UnitSystem;

});
