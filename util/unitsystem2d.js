define([], function() {

    /**
     * Works akin to the other unit system, but has different dimensions for X/Y.
     * Used for rectangular systems (such as a grid) that may not have even/square x/y.
     */
    class UnitSystem2d {
        /**
         * Creates a unit system width rows and columns
         * @param rows the rows of the grid
         * @param cols the columns of the grid
         * @param width the width of the grid
         * @param height the height of the grid
         */
        constructor(rows, cols, width, height) {
            this.setRows(rows);
            this.setCols(cols);
            this.setWidth(width);
            this.setHeight(height);
        }

        setWidth(width) {
            this.width = width;
            if (this.cols)
                this.pixelsPerUnitX = this.width / this.cols;
        }

        setHeight(height) {
            this.height = height;
            if (this.rows)
                this.pixelsPerUnitY = this.height / this.rows;
        }

        setRows(rows) {
            this.rows = rows;
            if (this.height)
                this.pixelsPerUnitY = this.height / this.rows;
        }

        setCols(cols) {
            this.cols = cols;
            if (this.width)
                this.pixelsPerUnitX = this.width / this.cols;
        }

        pixelsToUnitsX(px) {
            return px / this.pixelsPerUnitX;
        }
        pixelsToUnitsY(px) {
            return px / this.pixelsPerUnitY;
        }

        unitsToPixelsX(units) {
            return units * this.pixelsPerUnitX;
        }
        unitsToPixelsY(units) {
            return units * this.pixelsPerUnitY;
        }


    }

    return UnitSystem2d;

});
