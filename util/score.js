define([], function() {

    /**
     * Keeps track of a single integer, stores it locally, and can handle high score functionality.
     * A nice utility that also interacts with the End game state.
     */
    class Score {
        constructor(storageName, x, y) {
            this.storageName = storageName;
            this.x = x;
            this.y = y;
            this.value = 0;
        }

        /** Loads the score from local JS storage */
        load() {
            this.value = Number(localStorage.getItem(this.storageName));
            if (this.value === null) {
                // Initialize the value as 0.
                localStorage.setItem(this.storageName, 0);
                this.value = 0;
            }
            return this.value;
        }

        /** Saves the score from local JS storage. Returns true if high score. */
        save() {
            let stored = Number(localStorage.getItem(this.storageName));
            if (stored != null && this.value > stored) {
                localStorage.setItem(this.storageName, this.value);
                return true;
            }
            return false;
        }

        /** Draws the score */
        draw(ctx) {
            ctx.font = '30px serif';
            ctx.fillStyle = 'black';
            ctx.textAlign = "start";
            ctx.fillText('Score: ' + this.value, this.x, this.y);
        }
    }
    Score.HEIGHT = 20;

    return Score;
});